terragrunt = {
  terraform {
    source = "../../../modules//asg-with-ebs"
  }

  include = {
    path = "${find_in_parent_folders()}"
  }

  # Vpc should be up and running
  dependencies {
    paths = ["../alb", "../vpc"]
  }
}

# Define vpc information
aws_remote_region = "us-east-1"
vpc_remote_state_bucket = "terragrunt-lysimon"
vpc_remote_state_key = "lysimon/us-east-1/vpc/terraform.tfstate"

min_asg_count = "0"
max_asg_count = "0"

spinnaker_version = "1.9.5"

availability_zone = "us-east-1a"

s3_spinnaker_bucket_name="some-spinnaker-bucket-name"

spinnaker_public_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMar532T7vQyEoS1poc/H5QfSEJ2vagJ5bgf8wtNDxiJbQLpkqCHWGcOq1w0LssgBlZbgCdS73lON7dZ7Aa5iAl8NASNdQyOWHReC2Gdy3wgZ74rGAEwpszQZcrN/XLOgiCH2sUzk8mUJhOJhNJ74f7HvhTmBB5DtnlS7DnllLdwV1f4nGctazQCMgEgcMSQWoZWxvbHSQpOww9wZJy5T3w0DjhqiLE/RDzKrIXQ5G9qmmGNFRvOew4HgxEXZy3jR1K0D+dkwCFHrI7k28kfJu1g2rylHtaAL1ITBw9lHraFvZ49DgNwOI0sKaw0/YIilaf0A7PDhed5hmJqGanLHX laurent@laurent-X550LD"
