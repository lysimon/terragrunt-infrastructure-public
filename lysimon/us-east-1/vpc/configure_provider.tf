# Using modules from terraform registry means that we have to defined those blocks.
# Check https://github.com/gruntwork-io/terragrunt/issues/311
# https://gist.github.com/antonbabenko/2ca1225589c7c6d42f476f97d779d4ff
# For more information

# We can add tf file here that will be added to the module.
# If you have more than one module, it is better to just
provider "aws" {
  region     = "us-east-1"
}

# Need to define remote state, it will fail otherwise.
# See https://github.com/gruntwork-io/terragrunt/issues/230
terraform {
  # The offending block. The configuration for this backend will be filled in by Terragrunt.
  backend "s3" {}
}
