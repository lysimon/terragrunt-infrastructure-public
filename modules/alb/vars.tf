variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
}

terraform {
  # The offending block. The configuration for this backend will be filled in by Terragrunt.
  backend "s3" {}
}

variable "alb_name" {
default = "default-alb"
  description = "The name for the ecs cluster. This name is also used to namespace all the other resources created by this module."
}

variable "route53_hosted_zone_id" {}
variable "route53_hosted_zone_name" {}

variable "allowed_cidr" {
  description = "From where the load balancer should be accessible"

    default = []
    type    = "list"
}

# Reading remote state file
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.vpc_remote_state_key}"
    region = "${var.remote_state_region}"
  }
}

variable "remote_state_region" {}

variable "remote_state_bucket" {
  description = "The name of the S3 bucket for the database's remote state"
}

variable "vpc_remote_state_key" {
  description = "The path for the database's remote state in S3"
}
