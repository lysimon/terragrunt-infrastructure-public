variable "remote_state_bucket" {}
variable "alb_remote_state_key" {}
variable "vpc_remote_state_key" {}
variable "remote_state_region" {}

# Reading remote state file
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.vpc_remote_state_key}"
    region = "${var.remote_state_region}"
  }
}

# Reading remote state file
data "terraform_remote_state" "alb" {
  backend = "s3"

  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.alb_remote_state_key}"
    region = "${var.remote_state_region}"
  }
}

variable "route53_hosted_zone_id" {}
