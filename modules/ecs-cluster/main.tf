# Configure aws provider here
provider "aws" {
  region = "${var.aws_region}"
}

# Use default s3 backend
terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# Create the cluster
resource "aws_ecs_cluster" "default" {
  name = "${data.terraform_remote_state.vpc.vpc_main_route_table_id}"
  #name = "${var.cluster_name}"
}

# Create the ECR cluster repository
resource "aws_ecr_repository" "default" {
  name = "${aws_ecs_cluster.default.name}"
}
